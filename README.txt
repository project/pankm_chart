
INTRODUCTION
-------------
* A module is a set of PHP, JavaScript, and CSS files that extends 
  site features and adds functionality.

* You can turn the features and functionality on by installing the module,
  and you can turn it off by uninstalling the module.

* Pankm chart module enables to draw a chart based on the uploaded CSV file.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. 
   Visit https://www.drupal.org/docs/user_guide/en/extend-module-install.html 
   for further information.

 * Please check the CONFIGURATION for the external libraries.

PURPOSE
---------
* User should able to copy the embed code and can use the chart in 
  other article and blogs.

CONFIGURATION
-------------
* After downloading the module please add the following three 
  external libraries in the folder "/pankm_chart/js/Scripts/"

* 1) d3.js 
     from https://d3js.org and the Version ---> v5.9.2
* 2) polyfill.min.js from 
     https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js
* 3) fetch.umd.js from  
     https://github.com/github/fetch/releases/download/v3.0.0/fetch.umd.js

* Install the module and visit the sample node created in "/admin/content".

* For creating a new node visit the node add page "/node/add/pankm_chart".

* Need the data file(CSV)(Must have a default sample file) to add a new node. 
  User should be able to provide data in CSV format similar to the sample CSV.
 
REQUIREMENTS
---------------
* The following three external files are required. 
  It needs to be download separately from the external link.

* d3.js, polyfill.min.js, fetch.umd.js

CURRENT FUNCTIONALITY
----------------------

1) When installing the module the required content type 
  and a sample node will be created automatically.

2) The user can upload their own CSV file simple to the sample.csv.
   The chart will plot automatically.

3) User can select the required chart type. Currently implemented chart types 
  are Bar Chart, Line Chart, Multi-Line Chart, and Pie Chart.

4) The max CSV file row limit is 100 lines.

5) There are some options to Copy Link,Print the page and get the Embedded code.


WORKING STRUCTURE
------------------
* A sample node is created at the time of module installation. 
  The content type is created using a yml file in the install folder. 
  Fields are created programmatically using the .install file.

* The CSV file will directly be fetched from the directory.
  No conversion of the CSV file.

* The node page will display the Title of the chart and under that,
  the chart will be drawn. 

* The description will be shown above the chart. A share option will be shown. 

* Upon clicking the share option user will be given the embed code and another 
  option to copy the URL and print the page.

* The embed code will contain iframe with src pointing to the corresponding 
  node page. This URL will have an additional parameter which will indicate 
  that the page should be shown in an embed format. If the parameter is 
  present the chart will be drawn in full width, the title and description 
  can be hidden. On mouse hover, an icon should be shown. On clicking that 
  icon the user will be redirected to the chart node page.
