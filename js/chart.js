/* global d3, drupalSettings, multiChart, pieChart, barChart  */
/**
 * @file
 * Chart object.
 */

// eslint-disable-next-line no-unused-vars
function Chart(container, data) {
  'use strict';
  this.chartType = drupalSettings.chartType;
  this.mapData = data;
  this.container = container;
  this.panContainer = d3.select(container);
  this.chartWrap = 'chart-wrapper';
  this.setChart = function () {
    var share;
    var sharePanelData;
    var shareItems;
    var iframeContent;
    var copyLink;
    var windowUrl;
    var windowLink;
    var container;
    var embededLink;
    var shareWrap;
    container = this.container;
    if (this.mapData.length < 30) {
      this.panContainer.classed('less-than-30', true);
    }
    else if (this.mapData.length > 60 && this.mapData.length < 80) {
      this.panContainer.classed('greater-than-60', true);
    }
    else if (this.mapData.length > 80) {
      this.panContainer.classed('greater-than-80', true);
    }
    else {
      this.panContainer.classed('between-30-60', true);
    }

    sharePanelData = [{
      iconClass: 'copy-icon',
      heading: 'Copy Link',
      desc: 'Click here to copy the site url',
      className: 'copy-link'
    }, {
      iconClass: 'print-icon',
      heading: 'Print',
      desc: 'Click here to print the chart',
      className: 'print-link'
    }, {
      iconClass: 'embed-icon',
      heading: 'Embeded Link',
      desc: 'Embed this chart any where on your site',
      className: 'embed-link'
    }];

    /* Urls used inside share panel */
    windowLink = window.location.href; // For copy link.
    windowUrl = '//' + location.host + location.pathname; // For Embed iframe link.
    embededLink = windowUrl + '?pankm_embed=1';

    /* Chart container creation */

    d3.select(container)
      .append('div')
      .attr('class', 'chart')
      .append('div')
      .attr('class', 'share-wrap');
    d3.select(insideChart(container, '.chart'))
      .append('div')
      .attr('class', this.chartWrap);

    /* Append Share items to the page */

    shareWrap = d3.select(insideChart(container, '.share-wrap'));
    shareWrap.append('a')
      .attr('class', 'share-link')
      .text('share')
      .on('click', function () {
        openCloseSharePanel(this);
      });
    share = shareWrap
      .append('div')
      .attr('class', 'share');
    shareItems = share
      .selectAll('div')
      .data(sharePanelData)
      .enter()
      .append('div')
      .attr('class', function (d) {
        return d.className;
      });
    shareItems.append('span')
      .attr('class', function (d) {
        return d.iconClass;
      });
    shareItems.append('div')
      .append('p')
      .text(function (d) {
        return d.heading;
      })
      .attr('class', 'heading');
    shareItems.selectAll('div').append('p').text(function (d) {
      return d.desc;
    }).attr('class', 'desc');

    /* Append copy link to the sharePanel */

    copyLink = d3.select(insideChart(container, '.copy-link'))
      .on('click', function () {
        // Var input =d3.select(this).select('.dummy-input');
        var input = document.getElementsByClassName(container.slice(1, container.length))[0]
          .getElementsByClassName('dummy-input')[0];
        if (input.value !== ' ') {
          copyText(input);
        }
      });
    copyLink.select('div')
      .append('input')
      .attr('class', 'dummy-input')
      .property('value', windowLink);
    copyLink.select('.heading')
      .append('span')
      .attr('class', 'tick');

    /* Append iframe  to the sharePanel */

    iframeContent = "<div class='iframe-container'><a target='_blank' href='" + windowUrl + "'>Visit node</a><iframe id='frame' class='custom-iframe' src='" + embededLink + "' ></iframe></div>";
    d3.select(insideChart(container, '.embed-link div'))
      .append('div')
      .attr('class', 'iframe-wrap clearfix')
      .append('textarea')
      .text(iframeContent)
      .attr('class', 'input-embed');
    d3.select(insideChart(container, '.iframe-wrap'))
      .append('a')
      .attr('class', 'copy-embed')
      .text('Copy')
      .on('click', function () {
        var input = document.getElementsByClassName(container.slice(1, container.length))[0]
          .getElementsByClassName('input-embed')[0];
        copyText(input);
      });
    d3.select(insideChart(container, '.print-link'))
      .on('click', function () {
        printChart(container);
      });

    /**
         * [To return a query selector ]
         * @param  {string} wrapper  Wrapper element
         * @param  {string} selector Element inside wrapper
         * @return {string}          The complete query selector
         */
    function insideChart(wrapper, selector) {
      return wrapper + ' ' + selector;
    }

    /**
         * To open and close the Share panel
         * @param  {object} item Clicked item
         */
    function openCloseSharePanel(item) {
      var shareDiv = d3.select(item.parentNode)
        .select('.share');
      shareDiv.classed('open', !shareDiv.classed('open'));
    }

    /**
         * Copy the text from input to clipboard
         * Dummy input is used for copy site url
         * @param  {object} input Input element to be copied
         */
    function copyText(input) {
      input.select();
      document.execCommand('copy');
      if (input.className === 'dummy-input') {
        var tick = d3.select(insideChart(container, '.heading .tick'));
        tick.classed('success', !tick.classed('success'));
        setTimeout(function () {
          tick.classed('success', !tick.classed('success'));
        }, 1000);
      }
    }

    /**
         * Print the chart
         * A custom iframe with class print-frame is used to print the chart
         * @param  {string} container chart container to be printed
         */
    function printChart(container) {
      var iFrame = document.getElementById('print-frame');
      var contentToPrint = document.querySelector(container + ' .chart-wrapper').innerHTML;
      iFrame.contentDocument.body.innerHTML = contentToPrint;
      iFrame.style.display = 'block';
      iFrame.contentWindow.window.focus();
      setTimeout(function () {
        iFrame.contentWindow.window.print();
      }, 1000);
      iFrame.style.display = 'none';
      setTimeout(function () {
        iFrame.contentDocument.body.innerHTML = '';
      }, 5000);
    }
  };

  /**
     * For calculate the dimension for plot the chart
     * Dimensions include other details about the chart(eg:wrapper for the chart,etc...)
     * @return {object} Dimensions
     */
  this.calcDimension = function () {
    var dimensions; var width; var height; var margin = {
      top: 20,
      right: 20,
      bottom: 30,
      left: 40
    };
    var containerWidth = this.panContainer.node().offsetWidth;
    width = containerWidth - margin.left - margin.right;
    height = (parseInt(containerWidth * 0.52)) - margin.top - margin.bottom;
    dimensions = {
      width: width,
      height: height,
      chartWrap: '.' + this.chartWrap,
      chartType: this.chartType,
      margin: margin,
      container: this.container
    };

    return dimensions;
  };

  /**
     * Draw the graph based on dimensions and details
     * @param  {object} dimensions Dimensions and details about chart
     */
  this.drawGraph = function (dimensions) {
    switch (dimensions.chartType) {
      case 'line':
      case 'multi-line':
        multiChart(dimensions, data);
        break;

      case 'pie':
        pieChart(dimensions, this.mapData);
        break;

      case 'bar':
        barChart(dimensions, this.mapData);
        break;
    }
  };

  /**
     * Redraw the graph on window resize
     */
  this.redrawGraph = function () {
    var s = d3.selectAll(this.container + ' svg');
    s.remove();
    this.drawGraph(this.calcDimension());
  };
}
