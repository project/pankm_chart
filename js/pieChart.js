/* global d3, pankmColors */

/**
 * @file
 * Pie chart is drawn here.
 */

// eslint-disable-next-line no-unused-vars
function pieChart(dimensions, data) {
  'use strict';
  var svg; var pie; var arc; var arcs; var radius;
  var g; var color;
  var width = dimensions.width;
  var height = dimensions.height;
  var margin = dimensions.margin;
  var columns = data['columns'];
  svg = d3.select(dimensions.container)
    .select(dimensions.chartWrap)
    .append('svg')
    .attr('class', 'pie-chart')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
  // Tooltip for showing the data when hovering the lines.
  var tooltip = d3.select('.pankm-tooltip');
  radius = Math.min(width, height) / 2;
  g = svg.append('g')
    .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
  color = d3.scaleOrdinal()
    .domain(data)
    .range(pankmColors());
  // Generate the pie.
  pie = d3.pie()
    .sort(null);
  // Generate the arcs.
  arc = d3.arc()
    .innerRadius(0)
    .outerRadius(radius);
  // Generate groups.
  arcs = g.selectAll('arc')
    .data(pie(data.map(function (d) {
      return parseFloat(d[columns[1]]);
    })))
    .enter()
    .append('g')
    .attr('class', 'arc');
  arcs.append('path')
    .attr('fill', function (d, i) {
      return color(i);
    })
    .attr('d', arc)
    .on('mouseover', function (d, i) {
      tooltip.classed('visible', true);
      tooltip
        .html('<strong>' + columns[0] + '</strong>' + ' : ' + data[i][columns[0]] + '<br>' + '<strong>' + columns[1] + '</strong>' + ' : ' + d.data)
        .style('left', (d3.event.pageX - 34) + 'px')
        .style('top', (d3.event.pageY - 12) + 'px');
      d3.select(this)
        .attr('fill', 'lightblue');
    })
    .on('mouseout', function (d, i) {
      tooltip.classed('visible', false);
      d3.select(this)
        .attr('fill', color(i));
    });
  // Remove legends on resize.
  d3.select('.pankm-legend-wrap').remove();
  console.log(dimensions.container);
  var legendWrap = d3.select(dimensions.container + ' .chart-wrapper')
    .append('g')
    .attr('transform', `translate(0,${radius * 2 + 20})`)
    .classed('pankm-legend-wrap', true);
  // legendWrap.append("h3").text("Legends").classed('title',true);.
  legendWrap.append('div')
    .classed('pankm-legend', true);
  legendWrap.select('.pankm-legend')
    .selectAll('div')
    .data(data)
    .enter()
    .append('div')
    .html(function (d, i) {
      return "<span style='width: 15px;height:15px; background:" + color(i) + "'>" + '</span>' + "<div class='data'>" + d[columns[0]] + '<strong class="print-only">: ' + d[columns[1]] + '</strong></div>';
    });
}
