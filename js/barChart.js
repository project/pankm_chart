/* global d3, trimXYLabels, showTrimLabels */
/**
 * @file
 * Barchart is drawn here.
 */

// eslint-disable-next-line no-unused-vars
function barChart(dimensions, data) {
  'use strict';
  var svg;
  var x;
  var y;
  var tooltip;
  var width = dimensions.width;
  var height = dimensions.height;
  var margin = dimensions.margin;
  var columns = data['columns'];
  var xAxis = columns[0];
  var yAxis = columns[1];
  svg = d3.select(dimensions.container)
    .select(dimensions.chartWrap)
    .append('svg')
    .attr('class', 'bar-chart')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + (margin.left + 10) + ',' + margin.top + ')');
  // Tooltip.
  tooltip = d3.select('.pankm-tooltip');
  // Scale the range of the data in the domains.
  x = d3.scaleBand()
    .range([0, width])
    .padding(0.1);
  y = d3.scaleLinear()
    .range([height, 0]);
  x.domain(data.map(function (d) {
    return d[xAxis];
  }));
  y.domain([0, d3.max(data, function (d) {
    return parseFloat(d[yAxis]);
  })]);
  // Append the rectangles for the bar chart.
  svg.selectAll('.bar')
    .data(data)
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', function (d) {
      return x(d[xAxis]);
    })
    .attr('width', x.bandwidth())
    .attr('y', function (d) {
      return y(d[yAxis]);
    })
    .attr('height', function (d) {
      return height - y(d[yAxis]);
    })
    .on('mouseover', function (d, i) {
      tooltip.classed('visible', true);
      tooltip
        .html('<strong>' + xAxis + '</strong>' + ' : ' + d[xAxis] + '<br>' + '<strong>' + yAxis + '</strong>' + ' : ' + d[yAxis])
        .style('left', (d3.event.pageX - 34) + 'px')
        .style('top', (d3.event.pageY - 12) + 'px');
    })
    .on('mouseout', function () {
      tooltip.classed('visible', false);
    });
  // Add the x Axis.
  svg.append('g')
    .attr('class', 'x-axis')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(x));
  // Add the y Axis.
  svg.append('g')
    .call(d3.axisLeft(y));
  var wrapperHeight = d3.select('.bar-chart').node().getBoundingClientRect().height;
  d3.select('.bar-chart')
    .append('text')
    .attr('transform', 'translate(' + (width / 2) + ',' + (wrapperHeight + 50) + ')')
    .attr('text-anchor', 'middle')
    .text(columns[0]);
  d3.select('.bar-chart')
    .append('text')
    .attr('transform', 'translate(' + 10 + ',' + (wrapperHeight / 2) + ')rotate(-90)')
    .attr('text-anchor', 'middle')
    .text(columns[1]);
  wrapperHeight = d3.select('.bar-chart g').node().getBoundingClientRect().height;
  d3.select('.bar-chart').attr('height', wrapperHeight + 85);

  /* Trim length x and y axis labels */
  trimXYLabels();
  showTrimLabels(tooltip);
}
