/* global drupalSettings, d3, Chart */
/**
 * @file
 * Chart creation and other validations goes here.
 */

// eslint-disable-next-line no-console
(function (d3) {
  'use strict';
  var csvPath;
  var c1;
  var cssPath;
  var tab;

  /* if pankm_embed=1 in url --> A class  pankm-embed is added to body */
  tab = urlParamCheck('pankm_embed');
  commonComponents();
  switch (tab) {
    case '1':
      addPankmEmbed();
      break;

    default:
      chartPrintFrame();
      break;
  }

  /* Chart Creation */
  csvPath = drupalSettings.csvPath;
  cssPath = drupalSettings.modulePath + '/css/style.css';
  d3.csv(csvPath).then(function (data) {
    c1 = new Chart('.pan-chart', data);
    c1.setChart();
    c1.drawGraph(c1.calcDimension());
  });

  /* if body has class pankm-embed  chart is only displayed */
  if (d3.select('body').classed('pankm-embed')) {
    embedChartDisplay();
  }

  /* resize Chart code */
  var resizeTimer;
  // eslint-disable-next-line no-console
  window.onresize = function () {
    clearTimeout(resizeTimer);
    // eslint-disable-next-line no-console
    resizeTimer = setTimeout(function () {
      c1.redrawGraph();
    }, 100);
  };

  /* Functions */
  /**
     * Creating common components for all charts.
     */
  function commonComponents() {
    // Tooltip for showing the data when hovering the lines.
    d3.select('body').append('div').classed('pankm-tooltip', true);
  }

  /**
     * To check url parameters.
     *
     * @param {string} name Parameter in url eg: pankm_embed
     *
     * @return {string}      parameter value eg: 1 or 0
     */
  function urlParamCheck(name) {
    // eslint-disable-next-line no-useless-escape
    var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
    if (results == null) {
      return null;
    }
    else {
      return results[1] || 0;
    }
  }

  /*
  * Add body a class pankm-embed.
  */
  function addPankmEmbed() {
    d3.select('body').attr('class', 'pankm-embed');
  }

  /**
     * Adding a print frame for printing the chart
     * Used in printChart() in chart.js.
     */

  // eslint-disable-next-line no-console
  function chartPrintFrame() {
    d3.select('body').insert('iframe', ':first-child').attr('id', 'print-frame').attr('name', 'print_frame').attr('src', 'about:blank').style('display', 'none');
    setTimeout(function () {
      var iFrame = document.getElementById('print-frame');
      iFrame.contentDocument.head.innerHTML = '<link rel="stylesheet" media="all" href="' + cssPath + '">';
    }, 2000);
  }

  /**
     * Display only the chart for embed url.
     */
  function embedChartDisplay() {
    var panChart = document.getElementsByClassName('pan-chart')[0];
    var tooltip = document.getElementsByClassName('pankm-tooltip')[0];
    document.body.innerHTML = ' ';
    document.body.appendChild(tooltip);
    var container = document.createElement('div');
    container.className = 'center-chart';
    document.body.appendChild(container);
    document.getElementsByClassName('center-chart')[0].appendChild(panChart);
  }

  // eslint-disable-next-line no-console
  window.onload = function () {
    d3.select('body.page-node-type-pankm-chart').style('visibility', 'visible');
  };
}(d3));
