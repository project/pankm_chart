/* global d3 */
/**
 * @file
 * Common Functions used for all type of chart.
 */

// eslint-disable-next-line no-unused-vars
function trimXYLabels() {
  'use strict';

  /* Trim length x and y axis labels */
  d3.select('.x-axis').selectAll('text').each(function () {
    var textValue = d3.select(this);
    textValue.attr('transform', 'rotate(-45)');
    if (textValue.text().length > 7) {
      textValue.classed('trim', true);
      textValue.text(textValue.text().slice(0, 7) + '...');
    }
  });
  d3.select('.y-axis').selectAll('text').each(function () {
    var textValue = d3.select(this);
    if (textValue.text().length > 3) {
      textValue.classed('trim', true);
      textValue.text(textValue.text().slice(0, 3) + '...');
    }
  });
}

// eslint-disable-next-line no-unused-vars
function showTrimLabels(tooltip) {
  'use strict';
  d3.selectAll('.x-axis text.trim').on('mouseover', handleMouseOver).on('mouseout', handleMouseOut);
  d3.selectAll('.y-axis text.trim').on('mouseover', handleMouseOver).on('mouseout', handleMouseOut);

  function handleMouseOver(d) {
    tooltip.classed('visible', true);
    tooltip.html('<span>' + d + '</span>').style('left', (d3.event.pageX - 34) + 'px').style('top', (d3.event.pageY - 12) + 'px');
  }

  function handleMouseOut() {
    tooltip.classed('visible', false);
  }
}

// eslint-disable-next-line no-unused-vars
function pankmColors() {
  'use strict';
  var colors = ['#66c2a5', '#fc8d62', '#8da0cb', '#e78ac3', '#a6d854', '#ffd92f', '#e5c494', '#b3b3b3', '#ff7c43', '#ffa600', '#245c34', '#32733a', '#448a3d', '#5aa23e', '#74b93d', '#91d138', '#b2e830', '#d6ff21', '#fe4a49', '#2ab7ca', '#fed766', '#e6e6ea', '#f4f4f8', '#011f4b', '#03396c', '#005b96', '#6497b1', '#b3cde0', '#051e3e', '#251e3e', '#451e3e', '#651e3e', '#851e3e', '#4a4e4d', '#0e9aa7', '#3da4ab', '#f6cd61', '#fe8a71', '#2a4d69', '#4b86b4', '#adcbe3', '#e7eff6', '#63ace5', '#fe9c8f', '#feb2a8', '#fec8c1', '#fad9c1', '#f9caa7', '#009688', '#35a79c', '#54b2a9', '#65c3ba', '#83d0c9', '#ee4035', '#f37736', '#fdf498', '#7bc043', '#0392cf', '#d0e1f9', '#4d648d', '#283655', '#1e1f26', '#e0a899', '#c99789', '#96ceb4', '#ffeead', '#ff6f69', '#ffcc5c', '#88d8b0', '#6e7f80', '#536872', '#708090', '#36454f', '#4b3832', '#854442', '#fff4e6', '#3c2f2f', '#be9b7b', '#3b5998', '#8b9dc3', '#dfe3ee', '#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94', '#bdeaee', '#76b4bd', '#58668b', '#5e5656', '#ff77aa', '#ff99cc', '#ffbbee', '#ff5588', '#ff3377', '#edc951', '#eb6841', '#cc2a36', '#4f372d', '#00a0b0', '#84c1ff', '#add6ff', '#d6eaff', '#8d5524', '#c68642', '#e0ac69', '#f1c27d', '#ffdbac', '#bfd6f6', '#8dbdff', '#64a1f4', '#4a91f2', '#3b7dd8'];
  return colors;
}
