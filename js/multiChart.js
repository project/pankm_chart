/* global d3, trimXYLabels, showTrimLabels, pankmColors */
/**
 * @file
 * Multiline chart is drawn here.
 */

// eslint-disable-next-line no-unused-vars
function multiChart(dimensions, data) {
  'use strict';
  var svg;
  var x;
  var y;
  var width = dimensions.width;
  var height = dimensions.height;
  var margin = dimensions.margin;
  var columns = data['columns'];
  var columnsLength = columns.length;
  var columVals = [];
  function mapData(column) {
    // eslint-disable-next-line
    for (var key in data) {
      var pushData = data[key][column];
      // eslint-disable-next-line
      if (pushData !== undefined) {
        columVals.push(parseFloat(pushData));
      }
    }
  }
  var i;
  for (i = 1; i < columnsLength; i++) {
    mapData(columns[i]);
  }
  svg = d3.select(dimensions.container)
    .select(dimensions.chartWrap)
    .append('svg')
    .attr('class', 'line-chart')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + (margin.left + 10) + ',' + margin.top + ')');
  x = d3.scalePoint().range([0, width]);
  y = d3.scaleLinear().range([height, 0]);
  // Scale the range of the data in the domains.
  x.domain(data.map(function (d) {
    return d[columns[0]];
  }));
  y.domain([0, d3.max(columVals)]);
  // Ordinal color scale from d3.js.
  var color = d3.scaleOrdinal()
    .domain(data)
    .range(pankmColors());
    // Tooltip for showing the data when hovering the lines.
  var tooltip = d3.select('.pankm-tooltip');
  for (var key = 1; key < columnsLength; key++) {
    line(key);
    circle(key);
  }
  function line(key) {
    svg.append('path')
      .datum(data)
      .classed('data-line', true)
      .attr('d', d3.line()
        .x(function (d) {
          return x(d[columns[0]]);
        })
        .y(function (d) {
          return y(d[columns[key]]);
        }))
      .attr('fill', 'none')
      .attr('stroke', color(key))
      .attr('stroke-width', 2)
      .attr('id', columns[key].replace(/[^A-Z0-9]/ig, ''))
      .on('mouseover', mouseOver)
      .on('mouseout', mouseOut);
    function mouseOver() {
      d3.selectAll('.line-points')
        .attr('opacity', 0.2);

      d3.selectAll('.data-line')
        .attr('opacity', 0.2);
      d3.select(this)
        .attr('stroke-width', 4)
        .attr('opacity', 1);
      tooltip.classed('visible', true);
      tooltip
        .html('<strong>Line :</strong> ' + columns[key])
        .style('left', (d3.event.pageX - 34) + 'px')
        .style('top', (d3.event.pageY - 12) + 'px');
    }
    function mouseOut() {
      d3.selectAll('.line-points')
        .attr('opacity', 1);
      d3.selectAll('.data-line')
        .attr('opacity', 1);
      d3.select(this)
        .attr('stroke-width', 2)
        .attr('opacity', 1);
      tooltip.classed('visible', false);
    }
  }

  /* Adding the data nodes to the lines */
  function circle(key) {
    var linePoints = d3.select('.line-chart')
      .select('g')
      .append('g')
      .attr('class', 'line-points')
      .attr('id', 'circle-' + columns[key].replace(/[^A-Z0-9]/ig, ''));
    linePoints.selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
      .attr('cx', function (d) { return x(d[columns[0]]); })
      .attr('cy', function (d) { return y(d[columns[key]]); })
      .attr('r', function () { return 4; })
      .attr('fill', color(key))
      .on('mouseover', handleMouseOver)
      .on('mouseout', handleMouseOut);
    function handleMouseOver(d) { // Add interactivity.
      tooltip.classed('visible', true);
      tooltip
        .html('<strong>' + columns[0] + '</strong>' + ' : ' + d[columns[0]] + '<br>' + '<strong>' + columns[key] + '</strong>' + ' : ' + d[columns[key]])
        .style('left', (d3.event.pageX - 34) + 'px')
        .style('top', (d3.event.pageY - 12) + 'px');

      d3.select(this).attr('fill', 'lightblue');
    }

    function handleMouseOut() {
      tooltip.classed('visible', false);
      d3.select(this)
        .attr('fill', color(key));
    }
  }
  // Add the x Axis.
  svg.append('g')
    .attr('class', 'x-axis')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(x));
  // Add the y Axis.
  svg.append('g')
    .attr('class', 'y-axis')
    .call(d3.axisLeft(y));
  var wrapperHeight = d3.select('.line-chart').node().getBoundingClientRect().height;
  d3.select('.line-chart')
    .append('text')
    .attr('transform', 'translate(' + (width / 2) + ',' + (wrapperHeight + 50) + ')')
    .attr('text-anchor', 'middle')
    .text(columns[0]);
  if (columns.length < 3) {
    d3.select(dimensions.container).classed('not-multi', true);
    d3.select('.line-chart')
      .append('text')
      .attr('transform', 'translate(' + 10 + ',' + (wrapperHeight / 2) + ')rotate(-90)')
      .attr('text-anchor', 'middle')
      .text(columns[1]);
  }

  wrapperHeight = d3.select('.line-chart g').node().getBoundingClientRect().height;
  d3.select('.line-chart').attr('height', wrapperHeight + 80);
  trimXYLabels();
  showTrimLabels(tooltip);
  d3.select('.pankm-legend-wrap').remove();
  var legendWrap = d3.select(dimensions.container + ' .chart-wrapper')
    .append('div')
    .classed('pankm-legend-wrap', true);
  // legendWrap.append("h3").text("Legends").classed('title',true);.
  legendWrap.append('div')
    .classed('pankm-legend line-chart', true);
  legendWrap.select('.pankm-legend')
    .selectAll('div')
    .data(columns.slice(1, columnsLength))
    .enter()
    .append('div')
    .classed('item', true)
    .html(function (d, i) {
      return "<span style='background:" + color(i + 1) + "'>" + '</span>' + "<div class='data'>" + d + '</div>';
    });

  d3.selectAll('.pankm-legend .item')
    .on('click', function (d) {
      var self = d3.select(this);
      self.classed('hide-legend', !self.classed('hide-legend'));
      var clickedLine = d3.select('#' + d.replace(/[^A-Z0-9]/ig, ''));
      var circlesWrap = d3.select('#circle-' + d.replace(/[^A-Z0-9]/ig, ''));
      clickedLine.classed('hide-line', !clickedLine.classed('hide-line'));
      circlesWrap.classed('hide-circle', !circlesWrap.classed('hide-circle'));
    });
}
